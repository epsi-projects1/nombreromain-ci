import {RomanConverter} from "../RomanConverter";

describe('RomanConverter', () => {
    it('should 1 be I', () => {
        expect(new RomanConverter().convert(1)).toBe("I");
    });

    it('should 2 be II', () => {
        expect(new RomanConverter().convert(2)).toBe("II");
    });

    it('should 3 be III', () => {
        expect(new RomanConverter().convert(3)).toBe("III");
    });

    it('should 4 be IV', () => {
        expect(new RomanConverter().convert(4)).toBe("IV");
    });


    it('should 5 be V', () => {
        expect(new RomanConverter().convert(5)).toBe("V");
    });

    it('should 10 be X', () => {
        expect(new RomanConverter().convert(10)).toBe("X");
    });

    it('should 25 be XXV', () => {
        expect(new RomanConverter().convert(25)).toBe("XXV");
    });

    it('should 36 be XXXVI', () => {
        expect(new RomanConverter().convert(36)).toBe("XXXVI");
    });

    it('should 50 be L', () => {
        expect(new RomanConverter().convert(50)).toBe("L");
    });

    it('should 100 be C', () => {
        expect(new RomanConverter().convert(100)).toBe("C");
    });

    it('should 500 be D', () => {
        expect(new RomanConverter().convert(500)).toBe("D");
    });

    it('should 1000 be M', () => {
        expect(new RomanConverter().convert(1000)).toBe("M");
    });

    it('should 1459 be MCDLIX', () => {
        expect(new RomanConverter().convert(1459)).toBe("MCDLIX");
    });

    it('should 1999 be MCMXCIX', () => {
        expect(new RomanConverter().convert(1999)).toBe("MCMXCIX");
    });

    it('should 2695 be MMDCXCV', () => {
        expect(new RomanConverter().convert(2695)).toBe("MMDCXCV");
    });

    it('should 3999 be MMMCMXCIX', () => {
        expect(new RomanConverter().convert(3999)).toBe("MMMCMXCIX");
    });

    it('should 4000 be MMMM', () => {
        expect(new RomanConverter().convert(4000)).toBe("MMMM");
    });
});
describe('NumberDecomposer', () => {
    it('should 1 be decomposed', () => {
        expect(new RomanConverter().decompose(1)).toEqual([1]);
    });
    it('should 5 be decomposed', () => {
        expect(new RomanConverter().decompose(2)).toEqual([2]);
    });
    it('should 10 be decomposed', () => {
        expect(new RomanConverter().decompose(10)).toEqual([0, 1]);
    });
    it('should 25 be decomposed', () => {
        expect(new RomanConverter().decompose(25)).toEqual([5, 2]);
    });
    it('should 36 be decomposed', () => {
        expect(new RomanConverter().decompose(36)).toEqual([6, 3]);
    });
    it('should 134 be decomposed', () => {
        expect(new RomanConverter().decompose(134)).toEqual([4, 3, 1]);
    });
    it('should 1459 be decomposed', () => {
        expect(new RomanConverter().decompose(1459)).toEqual([9, 5, 4, 1]);
    });
    it('should 5423565643 be decomposed', () => {
        expect(new RomanConverter().decompose(5423565643)).toEqual([3, 4, 6, 5, 6, 5, 3, 2, 4, 5]);
    });
    it('should 54235656473 be decomposed', () => {
        expect(new RomanConverter().decompose(54235656473)).toEqual([3, 7, 4, 6, 5, 6, 5, 3, 2, 4, 5]);
    });
});