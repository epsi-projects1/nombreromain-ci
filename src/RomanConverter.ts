export class RomanConverter {


    decompose(number: number): number[] {
        let list = []
        number.toString().split("").map((digit) => list.unshift(parseInt(digit)));
        return list;
    }


    private convertOnes(digit: number): string {
        if (digit < 4) {
            return "I".repeat(digit);
        }
        if (digit === 4) {
            return "IV";
        }
        if (digit === 5) {
            return "V";
        }
        if (digit > 5 && digit < 9) {
            return "V" + "I".repeat(digit - 5);
        }
        if (digit === 9) {
            return "IX";
        }
    }

    private convertTens(digit: number) {

        if (digit < 4) {
            return "X".repeat(digit);
        }
        if (digit === 4) {
            return "XL";
        }
        if (digit === 5) {
            return "L";
        }
        if (digit > 5 && digit < 9) {
            return "L" + "X".repeat(digit - 5);
        }
        if (digit === 9) {
            return "XC";
        }

        return "";
    }

    private convertHundreds(digit: number) {
        if (digit < 4) {
            return "C".repeat(digit);
        }
        if (digit === 4) {
            return "CD";
        }
        if (digit === 5) {
            return "D";
        }
        if (digit > 5 && digit < 9) {
            return "D" + "C".repeat(digit - 5);
        }
        if (digit === 9) {
            return "CM";
        }
        return "";
    }

    private convertThousands(digit: number) {
        return "M".repeat(digit);
    }

    convert(number: number): string {

        const digits = this.decompose(number);
        let result: string = "";

        // for each digit, convert to roman numeral
        for (let i = digits.length - 1; i >= 0; i--) {
            const digit = digits[i];
            if (i === 0) {
                console.log("convert ones" + digit)
                result += this.convertOnes(digit);
            }
            if (i === 1) {
                console.log("convert tens" + digit)
                result += this.convertTens(digit);
            }
            if (i === 2) {
                result += this.convertHundreds(digit);
            }
            if (i === 3) {
                result += this.convertThousands(digit);
            }
        }
        return result;
    }
}